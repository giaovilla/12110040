﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog5.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}