﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCuoiKy.Models;

namespace WebCuoiKy.Controllers
{
    public class BinhLuanController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /BinhLuan/

        public ActionResult Index()
        {
            var binhluans = db.BinhLuans.Include(b => b.themthaoluan);
            return View(binhluans.ToList());
        }

        //
        // GET: /BinhLuan/Details/5

        public ActionResult Details(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Create

        public ActionResult Create()
        {
            ViewBag.themThaoLuanID = new SelectList(db.ThaoLuans, "ID", "Title");
            return View();
        }

        //
        // POST: /BinhLuan/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BinhLuan binhluan)
        {
            if (ModelState.IsValid)
            {
                db.BinhLuans.Add(binhluan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.themThaoLuanID = new SelectList(db.ThaoLuans, "ID", "Title", binhluan.themThaoLuanID);
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            ViewBag.themThaoLuanID = new SelectList(db.ThaoLuans, "ID", "Title", binhluan.themThaoLuanID);
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BinhLuan binhluan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(binhluan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.themThaoLuanID = new SelectList(db.ThaoLuans, "ID", "Title", binhluan.themThaoLuanID);
            return View(binhluan);
        }

        //
        // GET: /BinhLuan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            if (binhluan == null)
            {
                return HttpNotFound();
            }
            return View(binhluan);
        }

        //
        // POST: /BinhLuan/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BinhLuan binhluan = db.BinhLuans.Find(id);
            db.BinhLuans.Remove(binhluan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}