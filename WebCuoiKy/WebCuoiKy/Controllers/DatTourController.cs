﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCuoiKy.Models;

namespace WebCuoiKy.Controllers
{
    public class DatTourController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /DatTour/

        public ActionResult Index()
        {
            return View(db.DatTours.ToList());
        }

        //
        // GET: /DatTour/Details/5

        public ActionResult Details(int id = 0)
        {
            DatTour dattour = db.DatTours.Find(id);
            if (dattour == null)
            {
                return HttpNotFound();
            }
            return View(dattour);
        }

        //
        // GET: /DatTour/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DatTour/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DatTour dattour)
        {
            if (ModelState.IsValid)
            {
                db.DatTours.Add(dattour);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(dattour);
        }

        //
        // GET: /DatTour/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DatTour dattour = db.DatTours.Find(id);
            if (dattour == null)
            {
                return HttpNotFound();
            }
            return View(dattour);
        }

        //
        // POST: /DatTour/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DatTour dattour)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dattour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dattour);
        }

        //
        // GET: /DatTour/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DatTour dattour = db.DatTours.Find(id);
            if (dattour == null)
            {
                return HttpNotFound();
            }
            return View(dattour);
        }

        //
        // POST: /DatTour/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DatTour dattour = db.DatTours.Find(id);
            db.DatTours.Remove(dattour);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}