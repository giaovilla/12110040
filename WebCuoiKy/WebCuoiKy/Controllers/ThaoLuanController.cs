﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCuoiKy.Models;

namespace WebCuoiKy.Controllers
{
    public class ThaoLuanController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /ThaoLuan/

        public ActionResult Index()
        {
            var thaoluans = db.ThaoLuans.Include(t => t.UserProfile);
            return View(thaoluans.ToList());
        }

        //
        // GET: /ThaoLuan/Details/5

        public ActionResult Details(int id = 0)
        {
            ThaoLuan thaoluan = db.ThaoLuans.Find(id);
            if (thaoluan == null)
            {
                return HttpNotFound();
            }
            return View(thaoluan);
        }

        //
        // GET: /ThaoLuan/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /ThaoLuan/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ThaoLuan thaoluan)
        {
            if (ModelState.IsValid)
            {
                db.ThaoLuans.Add(thaoluan);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", thaoluan.UserProfileUserID);
            return View(thaoluan);
        }

        //
        // GET: /ThaoLuan/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ThaoLuan thaoluan = db.ThaoLuans.Find(id);
            if (thaoluan == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", thaoluan.UserProfileUserID);
            return View(thaoluan);
        }

        //
        // POST: /ThaoLuan/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ThaoLuan thaoluan)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thaoluan).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", thaoluan.UserProfileUserID);
            return View(thaoluan);
        }

        //
        // GET: /ThaoLuan/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ThaoLuan thaoluan = db.ThaoLuans.Find(id);
            if (thaoluan == null)
            {
                return HttpNotFound();
            }
            return View(thaoluan);
        }

        //
        // POST: /ThaoLuan/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ThaoLuan thaoluan = db.ThaoLuans.Find(id);
            db.ThaoLuans.Remove(thaoluan);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}