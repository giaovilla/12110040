﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class Rating
    {
        public int id { set; get; }
        public int SoSao { set; get; }
        public int PostID { set; get; }
        public virtual Post post { set; get; }
    }
}