﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class LienHe
    {
        public int id { set; get; }
        public String HoTen { set; get; }
        public String Email { set; get; }
        public String DiaChi { set; get; }
        public String SDT { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
    }
}