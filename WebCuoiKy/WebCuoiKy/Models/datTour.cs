﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class DatTour
    {
        public int id { set; get; }
        public DateTime ngayKhoiHanh { set; get; }
        public DateTime ngayKetThuc { set; get; }
        public int soLuongNguoi { set; get; }
        public string yeuCauKhac { set; get; }
        public string hoTen { set; get; }
        public string diaChi { set; get; }
        public string sdt { set; get; }
        public string email { set; get; }
        public int thongTinTournID { set; get; }
    }
}