﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class ThaoLuan
    {
        public int ID { set; get; }
        //  [Required]
        //[StringLength(500, ErrorMessage = "nhập số ký tự trong khoảng 20->500 ký tự!", MinimumLength = 20)]
        public string Title { set; get; }
        //[StringLength(2000, ErrorMessage = "nhập Tối thiểu 50 ký tự!", MinimumLength = 50)]
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        //[DataType(DataType.DateTime, ErrorMessage = "Nhập ngày giờ!")]
        // public DateTime DateUpdate { set; get; }
        public int SoLike { set; get; }
        public int SoComment { set; get; }
        // public int AccountID { set; get; }
        // public virtual Account account { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }
        public virtual ICollection<BinhLuan> binhluan_1 { set; get; }
    }
}