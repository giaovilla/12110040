﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class Profile
    {
        //public int AccountID { set; get; }
        //  public int
        // public string AccounttenDangNhap { set; get; }
        public string fullname { set; get; }
        public DateTime ngaySinh { set; get; }
        public string email { set; get; }
        public string sdt { set; get; }
        public int diem { set; get; }
        public string level { set; get; }
        [Key]
        [ForeignKey("UserProfile")]
        public int UserProfileUserID { set; get; }
        public virtual UserProfile UserProfile { set; get; }
    }
}