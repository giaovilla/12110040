﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class BinhLuan
    {
        public int ID { set; get; }
        //  [Required]
        //[StringLength(1000, MinimumLength = 50, ErrorMessage = "Tối thiểu 50 kí tự!")]
        public string Author { set; get; }
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        // public DateTime DateUpdate { set; get; }
        // 
        public int themThaoLuanID { set; get; }
        public virtual ThaoLuan themthaoluan { set; get; }
    }
}