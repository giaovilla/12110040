﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class Comment
    {
        public int ID { set; get; }
        // [Required]
        // [StringLength(1000, MinimumLength = 5, ErrorMessage = "Tối thiểu 5 kí tự!")]
        public string Author { set; get; }
        public string Body { set; get; }
        public DateTime DateCreate { set; get; }
        public int lastTime
        {
            get
            {
                return (DateTime.Now - DateCreate).Minutes;
            }
        }

        public int PostID { set; get; }
        public virtual Post post { set; get; }
    }
}