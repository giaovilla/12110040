namespace WebCuoiKy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class l1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        ChuyenMucID = c.Int(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ChuyenMucs", t => t.ChuyenMucID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.ChuyenMucID)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.ChuyenMucs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        tenChuyenMuc = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Author = c.String(),
                        Body = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        SoSao = c.Int(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.ThaoLuans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        SoLike = c.Int(nullable: false),
                        SoComment = c.Int(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.BinhLuans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Author = c.String(),
                        Body = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        themThaoLuanID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ThaoLuans", t => t.themThaoLuanID, cascadeDelete: true)
                .Index(t => t.themThaoLuanID);
            
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        UserProfileUserID = c.Int(nullable: false),
                        fullname = c.String(),
                        ngaySinh = c.DateTime(nullable: false),
                        email = c.String(),
                        sdt = c.String(),
                        diem = c.Int(nullable: false),
                        level = c.String(),
                    })
                .PrimaryKey(t => t.UserProfileUserID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID)
                .Index(t => t.UserProfileUserID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Profiles", new[] { "UserProfileUserID" });
            DropIndex("dbo.BinhLuans", new[] { "themThaoLuanID" });
            DropIndex("dbo.ThaoLuans", new[] { "UserProfileUserID" });
            DropIndex("dbo.Ratings", new[] { "PostID" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "UserProfileUserID" });
            DropIndex("dbo.Posts", new[] { "ChuyenMucID" });
            DropForeignKey("dbo.Profiles", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.BinhLuans", "themThaoLuanID", "dbo.ThaoLuans");
            DropForeignKey("dbo.ThaoLuans", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.Ratings", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.Posts", "ChuyenMucID", "dbo.ChuyenMucs");
            DropTable("dbo.Profiles");
            DropTable("dbo.BinhLuans");
            DropTable("dbo.ThaoLuans");
            DropTable("dbo.Ratings");
            DropTable("dbo.Comments");
            DropTable("dbo.ChuyenMucs");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
