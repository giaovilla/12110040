namespace Blog2_t6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.baiviet", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.baiviet", "Body", c => c.String(maxLength: 2000));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false, maxLength: 1000));
            AlterColumn("dbo.Tags", "Context", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "FirstName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Accounts", "LastName", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "LastName", c => c.String());
            AlterColumn("dbo.Accounts", "FirstName", c => c.String());
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Tags", "Context", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.baiviet", "Body", c => c.String());
            AlterColumn("dbo.baiviet", "Title", c => c.String());
        }
    }
}
