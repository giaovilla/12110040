namespace Blog2_t6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Posts", "Account_ID", "dbo.Accounts");
            DropForeignKey("dbo.Posts", "Tag_TagID", "dbo.Tags");
            DropIndex("dbo.Posts", new[] { "Account_ID" });
            DropIndex("dbo.Posts", new[] { "Tag_TagID" });
            RenameColumn(table: "dbo.Posts", name: "Account_ID", newName: "AccountID");
            CreateTable(
                "dbo.Tag_Post",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
            AddForeignKey("dbo.Posts", "AccountID", "dbo.Accounts", "ID", cascadeDelete: true);
            CreateIndex("dbo.Posts", "AccountID");
            DropColumn("dbo.Posts", "Tag_TagID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Posts", "Tag_TagID", c => c.Int());
            DropIndex("dbo.Tag_Post", new[] { "TagID" });
            DropIndex("dbo.Tag_Post", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "AccountID" });
            DropForeignKey("dbo.Tag_Post", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_Post", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "AccountID", "dbo.Accounts");
            DropTable("dbo.Tag_Post");
            RenameColumn(table: "dbo.Posts", name: "AccountID", newName: "Account_ID");
            CreateIndex("dbo.Posts", "Tag_TagID");
            CreateIndex("dbo.Posts", "Account_ID");
            AddForeignKey("dbo.Posts", "Tag_TagID", "dbo.Tags", "TagID");
            AddForeignKey("dbo.Posts", "Account_ID", "dbo.Accounts", "ID");
        }
    }
}
